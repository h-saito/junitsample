/**
 *
 */
package jp.co.excite_software.sample.bean;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author excite
 *
 */
public class SampleBeanTest {

  private SampleBean target;

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    target = new SampleBean();
  }

  /**
   * {@link jp.co.excite_software.sample.SampleBean#process(java.lang.String)} のためのテスト・メソッド。
   */
  @Test
  public final void testProcess1() {
    String sampletext = "test";
    String actual = target.process(sampletext);
    String expected = "Submitted text is \"" + sampletext + "\" !!";
    assertThat(expected, equalTo(actual));
  }

  /**
   * {@link jp.co.excite_software.sample.SampleBean#process(java.lang.String)} のためのテスト・メソッド。
   */
  @Test
  public final void testProcess2() {
    String sampletext = "";
    String actual = target.process(sampletext);
    String expected = "Submitted text is empty...";
    assertThat(expected, equalTo(actual));
  }

  @Test
  public final void testFail() {
    fail("テスト失敗のサンプル");
  }
}
