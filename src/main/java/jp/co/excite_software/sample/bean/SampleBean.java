/**
 *
 */
package jp.co.excite_software.sample.bean;

/**
 * @author excite
 *
 */
public class SampleBean {

  /***
   *
   * @param param
   * @return
   */
  public String process(String param){
    if(param == null || "".equals(param)){
      return "Submitted text is empty...";
    }
    return "Submitted text is \"" + param + "\" !!";
  }
}
